package com.example.films_project;

public class Film {
    public String name;
    public int rating;
    public int id;

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public Film(String n, int r){
        this.name = n;
        this.rating = r;
    }
    public Film(int id,String n, int r){
        this.id = id;
        this.name = n;
        this.rating = r;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }
}
