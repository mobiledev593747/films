package com.example.films_project;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class MyAdapter extends BaseAdapter {
    Context cnt;
    LayoutInflater layoutInflater;
    ArrayList<Film> filmArrayList;
    public MyAdapter (Context context, ArrayList<Film> films){
        this.cnt = context;
        this.filmArrayList = films;
        this.layoutInflater =(LayoutInflater) cnt.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return filmArrayList.size();
    }
    @Override
    public Object getItem(int i) {
        return filmArrayList.get(i);
    }
    @Override
    public long getItemId(int i) {
        return filmArrayList.indexOf(i);
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if(view == null){ view = layoutInflater.inflate(R.layout.list_item,viewGroup,false);}
        TextView f_name =view.findViewById(R.id.film);
        f_name.setText(filmArrayList.get(i).getName());
        TextView f_rat = view.findViewById(R.id.rating);
        f_rat.setText("Rating " + filmArrayList.get(i).getRating() + "/10");
        final Film film = (Film)getItem(i);
        return view;
    }
}
