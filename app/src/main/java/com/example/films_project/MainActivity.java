package com.example.films_project;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    MyAdapter adapter;
    ListView listView;
    Button add;
    EditText name, rating;
    SharedPreferences sharedPreferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        add = findViewById(R.id.add_button);
        name = findViewById(R.id.film_name);
        rating = findViewById(R.id.film_rating);
        listView = findViewById(R.id.films_list);
        final DatabaseHelper_Films databaseHelper_films2 = new DatabaseHelper_Films(MainActivity.this);
        ShowList(databaseHelper_films2);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                /*
                Film chosen = (Film) listView.getItemAtPosition(i);
                sharedPreferences = getPreferences(MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("Saved_film",chosen.getName());
                editor.commit();
                Intent f = new Intent(MainActivity.this,Film_Comments.class);
                startActivity(f);

                 */
                Toast.makeText(getBaseContext(),"Click-clack",Toast.LENGTH_LONG).show();
            }
        });


        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name_ = name.getText().toString();
                int rat_ = Integer.parseInt(rating.getText().toString());
                if(name_ != null && 0 < rat_ &&  rat_ <= 10){
                    Film film = new Film(name_,rat_);
                    boolean s =databaseHelper_films2.addOne(film);
                    Toast.makeText(getBaseContext(), "item added: " + s, Toast.LENGTH_LONG).show();
                    ShowList(databaseHelper_films2);
                }else {
                    Toast.makeText(getBaseContext(), "Invalid data", Toast.LENGTH_LONG).show();
                }
            }
        });
    }
    public void ShowList(DatabaseHelper_Films databaseHelper_films){
        adapter= new MyAdapter(MainActivity.this,databaseHelper_films.getAll(MainActivity.this));
        listView.setAdapter(adapter);
    }
}